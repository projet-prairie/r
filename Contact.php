<?php

    function isPhone($var){ /* Fonction pour vérifier le téléphone */
      return preg_match("/^[0-9 ]*$/", $var);
    }

    function isEmail($var){ /* Fonction pour vérifer l'email */
      return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    function verifyInput($var){ /* Fonction sécurité */
      $var = trim($var); /* Supprime les espace inutiles */
      $var = stripslashes($var);  /* supprime les antislash */
      $var = htmlspecialchars($var); /* Convertit les caractères spéciaux */

      return $var;
  }

                  
$errornom = $errorprenom = $erroremail = $errortelephone = $errorsujet_du_message = $errorcontenu = ""; 
$nom = $prenom = $email = $telephone = $sujet_du_message = $contenu = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){

$nom =              verifyInput($_POST['nom']);
$prenom =           verifyInput($_POST['prenom']);
$email =            verifyInput($_POST['email']);
$telephone =        verifyInput($_POST['telephone']);
$sujet_du_message = verifyInput($_POST['sujet_du_message']);
$contenu =          verifyInput($_POST['contenu']);
$requestText = "";
$isSuccess = true;

  /* Vérification des champs */

  if (empty($nom)){
    $errornom = 'Merci de renseigner votre nom. <br/>';
    $isSuccess = false;}
      else {
        $requestText .= "`$nom`, ";
  }
  if (empty($prenom)){
      $errorprenom = 'Merci de renseigner votre prénom. <br/>';
      $isSuccess = false;}
      else {
        $requestText .= "`$prenom`, ";
      }
  if (!isEmail($email)){
    $erroremail = 'Merci de renseigner une adresse mail valide. <br/>';
    $isSuccess = false;}
    else {
      $requestText .= "`$email`, ";
    }
  if (!isPhone($telephone)){
    $errortelephone = 'Merci de renseigner un numéro de téléphone valide (seulement des chiffres). <br/>';
    $isSuccess = false;}
    else {
      $requestText .= "`$telephone`, ";
    }
  if (empty($sujet_du_message)){
    $errorsujet_du_message = 'Merci de renseigner le sujet de votre message. <br/>';
    $isSuccess = false;}
    else {
      $requestText .= "`$sujet_du_message`, ";
    }
  if (empty($contenu)){
    $errorcontenu = 'Oops vous avez oublié de nous écrire votre message ! <br/>';
    $isSuccess = false;}
    else {
      $requestText .= "`$contenu`";
    }
;

    if ($isSuccess)
    {
      $pdo = new PDO("mysql:host=localhost;dbname=R", "root", "root");
      $sqlRequest = "INSERT INTO contact (nom, prenom, email, telephone, sujet_du_message, contenu) VALUES ('$nom', '$prenom', '$email', '$telephone', '$sujet_du_message', '$contenu')";
      $request = $pdo -> prepare ($sqlRequest);
      $request -> execute(); /* envoi de la requête */

    }
}

?>



<!DOCTYPE html>
<html lang="fr-fr">
<head>


  <title>Contact - R</title>


</head>


<body onload="formulaire">


  <div class="Depart">
  <main>
    <?php include("Php/navbar.php"); ?>
   <h1 data-aos="fade-right">Nous contacter</h1>
  </div>
  <div id="ancre"></div>
<section>
<div class="merci" style="display:<?php if($isSuccess) echo'block'; else echo'none'; ?>">Votre message a bien été envoyé. Merci de nous avoir contactés ! :)</div>

    <div class="container" id="superbeAnimation">

        <div class="row">

          <div class="col-12">

          <form id="formulaire" action="#ancre" method="POST" novalidate>
          
         <div class="form-row">
            <div class="form-group col-md-6">
            <label for="Nom">Nom</label>
            <input type="text" class="form-control" id='exampleInputEmail1'aria-label="Entrez votre nom" placeholder="Ecrivez ici..." name="nom" value="<?php echo $nom; ?>">
            <p class="error_style"><?php echo $errornom ?></p>
            </div>
            <div class="form-group col-md-6">
            <label for="prenom">Prénom</label>
            <input type="text" class="form-control" id='exampleInputEmail1'aria-label="Entrez votre prénom" placeholder="Ecrivez ici..." name="prenom" value="<?php echo $prenom; ?>">
            <p class="error_style"><?php echo $errorprenom ?></p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-label="Entrez votre adresse mail" placeholder="Ecrivez ici... " name="email" value="<?php echo $email; ?>">
            <p class="error_style"><?php echo $erroremail ?></p>
            </div>
            <div class="form-group col-md-6">
            <label for="telephone">Telephone</label>
            <input type="text" class="form-control" id='exampleInputEmail1'aria-label="Entrez votre numéro de téléphone" placeholder="Ecrivez ici..." name="telephone" value="<?php echo $telephone; ?>">
            <p class="error_style"><?php echo $errortelephone ?></p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
            <label for="sujet_du_message">Sujet de votre message </label>
            <input type="text" class="form-control" id="exampleInputEmail1" aria-label="Entrez le sujet de votre message"  placeholder="Ecrivez ici..." name="sujet_du_message" value="<?php echo $sujet_du_message; ?>"> 
            <p class="error_style"><?php echo $errorsujet_du_message ?></p>
            </div>
          </div>          
          <div class="form-row">
            <div class="form-group col-md-12">
            <label for="contenu">Contenu du message</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" aria-label="Entrez votre message" placeholder="Ecrivez votre message ici..." name="contenu" ><?php echo $contenu; ?></textarea>
            <p class="error_style"><?php echo $errorcontenu ?></p>
            </div>
          </div>
           <input type="submit" value="Je valide"id="buttonposition" class="btnvalide primary ripple">
        </form>
        </div>
    </div>
  </div>
</section>

      
<footer>
  <?php include("Php/footer.php"); ?>
</footer>
</body>
<script src="Ressources/js/JavaContact.js" ></script>
</html>