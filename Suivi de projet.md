Timeline:

# Étapes

## Étape 1 : 2020-12-17
Réaliser les 4 pages en HTML CSS

## Étape 2 : 2020-12-21
Animer les pages avec du javascript

## Étape 3 : 2021-01-05
Charger les éléments (actualités, équipe, produit) depuis une bdd, les informations de contact doivent entrer dans la bdd

## Étape 4 : 2020-01-07
Finaliser le site

## Étape 5 : 2020-01-08
Debrief du projet

-------------------------------------

Brainstorming J1:

-Produit: De l'air

-Actualités:
	Présentation de la société, les derniers produits, les promos.
	

-Produits:
	Liste des produits
	Faux commentaires
	Fausse vidéo doublage.
	
-Equipe:
	Présentation des membres de l'équipe.
	
-Contact:
	Formulaire de contact
	

Accueil/actu: Kévin
Produits: Jordan
Equipe: Matthieu
Contact: Mélanie

Charte graphique:

Fond blanc
Nav bar bleu pastel

-------------------------------------

Idées des produits:

-Souffle de provence:

	Ambiance sonore: Cigales
	Odeur: Lavande, pastis.
	Emotion: Apaisement/vacances


-Tempête bretonne:

	Ambiance sonore: Pluie
	Odeur: Crêpes, caramel beurre salé, huîtres.
	Emotion: Nostalgie.


-Aeration New-Yorkaise:

	Ambiance sonore: Bruit des voitures
	Odeur: Polution et hot-dog.
	Emotion: Rêve américain


-Bourasque italienne:

	Ambiance sonore: Mamamia !
	Odeur: Pizza, Prosecco et Parmiggiano.
	Emotion: Envie de parler avec les mains.

-------------------------------------

Progression : 

* Comprendre Git [V]
* HTML/CSS [V]
* Javascript [V]
* PHP / BDD [V]

-------------------------------------

J16 - Changement de style graphique du site

Image de départ de toutes les pages avec artwork du jeu Firewatch.
Continuité du site en violet lorsqu'on scroll afin de faire croire
que l'image continue vers le bas de la page.

Couleurs:
Orange pastel, violet, rouge et blanc crème.

-------------------------------------
