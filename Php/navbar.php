<!DOCTYPE html>
<html lang="fr-fr">
<head>
 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width" initial-scale=1>


                                    <!--BOOTSTRAP-->


                                    <!-- FONTS -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Comfortaa&display=swap" rel="stylesheet"> 
                                    <!-- JQUERY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
                                    <!-- BOOTSTRAP -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                                    <!-- GLYPHICONS -->
<script src="https://kit.fontawesome.com/a493d15f17.js" crossorigin="anonymous"></script> 

                                    <!-- FANCYBOX -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


                                    <!-- TWEENMAX -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script> 
                                    <!-- AOS ANIM TOP -->
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script> 
                                    <!-- CSS -->

<link rel="stylesheet" type="text/css" href="Ressources/css/style_global.css">
<link rel="stylesheet" type="text/css" href="Ressources/css/style_home.css">
<link rel="stylesheet" type="text/css" href="Ressources/css/style_contact.css">
<link rel="stylesheet" type="text/css" href="Ressources/css/style_produits.css">
<link rel="stylesheet" type="text/css" href="Ressources/css/style_equipe.css">


</head>
<body>
<div class="espaceTop"></div>
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <a class="navbar-brand" href="index.php" data-toggle="tooltip" title="Aller vers la page accueil"><i class="fas fa-wind" id="logoNav"></i> R</a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls=".navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
   <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item" id="Index">
        <a class="nav-link" href="index.php" data-toggle="tooltip" title="Aller vers la page accueil">Accueil </a>
      </li>
      <li class="nav-item" id="Equipe">
        <a class="nav-link" href="equipe.php" data-toggle="tooltip" title="Aller vers la page équipe">Equipe</a>
      </li>
      <li class="nav-item" id="Produits">
        <a class="nav-link" href="Produits.php" data-toggle="tooltip" title="Aller vers la page produits">Produits</a>
      </li>
      <li class="nav-item" id="Contact">
        <a class="nav-link" href="Contact.php" data-toggle="tooltip" title="Aller vers la page contact">Contact</a>
      </li>
    </ul>
    <!--<span class="navbar-text">
      Le site qui vend de l'air !
    </span> -->
    </div>
  </nav>

  <script>

    function maFonction() { //Je déclare ma fonction maFontion.
      var url = document.URL; //Je declare ma variable url et je lui donne la valeur document.URL qui est le chemin de la page php sur laquelle nous sommes
      var pageSplitted = url.split('/'); //Je sépare le chemin a chaque / et met chaque partie dans un tableau que je nomme pageSplitted
      var page = pageSplitted[4]; //Je prend le cinquieme element du tableau que je stock dans ma variable page.


      switch(page) { //Switch
        case "index.php" : 
          document.getElementById('Index').classList.add('active');
          break;
        case "equipe.php" : 
          document.getElementById('Equipe').classList.add('active');
          break;
        case "Produits.php" : 
          document.getElementById('Produits').classList.add('active');
          break;
        case "Contact.php" : 
          document.getElementById('Contact').classList.add('active');
          break;
        default :
          document.getElementById('Index').classList.add('active');
      }
    }
    window.onload = maFonction; //Je lance ma fonction au chargement de la page.

  </script>

  </body>


</html>