<?php include("navbar.php"); ?>
<link rel="stylesheet" href="../Ressources/css/styles.css">
<?php
try{
  $pdo = new PDO("mysql:host=localhost;dbname=R", "root", "root");
  // Set the PDO error mode to exception
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
  die("ERROR: Could not connect. " . $e->getMessage());
}
try 
{
    $sql = "INSERT INTO contact (nom, prenom, email, telephone, sujet_du_message, contenu) VALUES (:nom, :prenom, :email, :telephone, :sujet_du_message, :contenu)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':nom', $_REQUEST['nom']);
    $stmt->bindParam(':prenom', $_REQUEST['prenom']);
    $stmt->bindParam(':email', $_REQUEST['email']);
    $stmt->bindParam(':telephone', $_REQUEST['telephone']);
    $stmt->bindParam(':sujet_du_message', $_REQUEST['sujet_du_message']);
    $stmt->bindParam(':contenu', $_REQUEST['contenu']);

    
    

    //Dump
$stmt->execute();
echo "Données insérées";
}
catch(PDOException $e){
  die ("Error:" . $e->getMessage());
}
unset($pdo);

?>

<p class="end_message">Votre message a bien été envoyé et sera traité dans les plus courts délais ! <br/>
  Merci et à bientôt !
</p>
<?php include("footer.php"); ?>