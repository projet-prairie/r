  <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>A propos</h6>
            <p class="text-justify">Tous les produits 'R' sont 100% bio-dégradables, 100% éco-responsables et sans huile de palme. Notre entreprise est très engagée dans le domaine de l'écologie car il en va de la survie de notre planète. Vous pouvez réutiliser les bocaux en verre après leur utilisation, à condition de bien les nettoyer car l'odeur et le bruit peuvent persister quelques temps.          </div>

          <div class="col-xs-6 col-md-3"></div>

          <div class="col-xs-5 col-md-2">
            <h6>Liens rapides</h6>
            <ul class="footer-links">
              <li><a href="index.php">Accueil</a></li>
              <li><a href="equipe.php">Notre équipe</a></li>
              <li><a href="Produits.php">Nos produits</a></li>
              <li><a href="Contact.php">Nous contacter</a></li>
            </ul>             
          </div>
          <div class="col-xs-1 col-md-1" style="font-size: 300%;">
          <a class="boutonup" href="#"><i class="fas fa-arrow-alt-circle-up"></i></a>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2020 Tous Droits Réservés.</p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="https://www.facebook.com/r/"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="https://twitter.com/r"><i class="fa fa-twitter"></i></a></li>
              <li><a class="instagram" href="https://instagram.com/r"><i class="fa fa-instagram"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
          
</footer>