<!DOCTYPE html>
<html lang="fr-fr">
<head>


  <title>Equipe - R</title>


</head>


<body>



  <div class="Depart">
  <main>
    <?php include("Php/navbar.php"); ?>
   <h1 data-aos="fade-right">Notre équipe</h1>
</div>

    <div class="container">
    <div class="boiteequipe" id="pave"> 
            <div class="equipe">Notre entreprise R est composée d’une équipe professionnelle, d’expérience et dynamique. Nous nous appuyons sur un réseau de partenaires freelances et de spécialistes de l'air, des odeurs et du bruit dans tout les pays du globe.
</br>Pour aboutir à un résultat optimal, chaque nouveau produit mis en vente est le résultat d'une étroite collaboration avec nos partenaires ainsi que la prise en compte des retours clients auxquels nous portons une attention particulière.
</br>C'est grâce à cela que nous vous proposons des produits de haute qualité.
</div>
        </div>
        
    <?php
try
{
       // On se connecte à MySQL et à la bonne bdd
       $bdd = new PDO('mysql:host=localhost;dbname=R', 'root', 'root');
}
catch(Exception $e)
{
       // En cas d'erreur, on affiche un message et on arrête tout
       die('Erreur : '.$e->getMessage());
}


// Si tout va bien, on peut continuer


// On récupère tout le contenu de la table equipe
$reponse = $bdd->query('SELECT * FROM equipe');


// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{
?>

<div class="row">
    <div class="col-12"> 
          
      </div>
      </div> 
                              <!--IMAGES + DESCRIPTION-->

    <div class="row" data-aos="fade-<?php echo $donnees['Direction']; ?>">
        <div class="col-lg-5 col-md-6 col-sm-4">
          <img src="<?php echo $donnees['Urlimage']; ?>" class="imgpersonne" alt="<?php echo $donnees['Alt']; ?>" >
        </div>
        <div class="col-lg-7 col-md-6 col-sm-5">
          <div class="description"><strong>ID :</strong> <?php echo $donnees['Identité']; ?> <br/><br/>
                                   <strong>POSTE : </strong> <?php echo $donnees['Poste']; ?> <br/><br/>
                                   <strong>NOTE :</strong> <?php echo $donnees['Note']; ?> </div>
        </div>
    </div>
    <?php
}
$reponse->closeCursor(); // Termine le traitement de la requête


?>   
</div>
  </main>



  <footer>
    <?php include("Php/footer.php"); ?>
  </footer>

</body>
<script src="Ressources/js/equipe.js"></script>
</html> 