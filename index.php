<!DOCTYPE html>
<html lang="fr-fr">
<head>


  <title>Bienvenue - R</title>


</head>


<body>

<div class="loader-container">

  <i class="fas fa-wind logoLoad" id="logoNav"></i>
  <div class="cirload"></div><div class="loader"></div>
</div>


  <div class="Depart">
    <?php include("Php/navbar.php"); ?>


    <h1 data-aos="fade-right">Bienvenue</h1>
</div>
<?php

try{
    $pdo = new PDO('mysql:host=localhost;dbname=R', 'root', 'root');
} 
catch(Exception $e) {
    echo "Impossible d'accéder à la base de données mySQL : ".$e->getMessage();
}

$promo = $pdo-> prepare("SELECT (Prix-(Prix*(promo/100)))AS totalpromo , `Titre`,`UrlImage`,`Alt`, `Sentiment`, `Prix`,`promo`,`Odeur`  FROM produits WHERE promo>0;");
$promo -> execute();
$products = $promo->fetchAll(PDO::FETCH_OBJ);


$news = $pdo-> prepare("SELECT * FROM `produits` ORDER BY ID DESC LIMIT 3;");
$news -> execute();
$nouveaux = $news->fetchAll(PDO::FETCH_OBJ);

?>

<main>


  <div class="container">
    <header id="headerprez">
      <span class="presentation"> <p>R, c'est une entreprise <strong>Française</strong>, portée par de jeunes entrepreneurs désireux de révolutionner le bien-être de son public. Avec des produits prélevés <strong> à la source par nos collaborateurs</strong>, nous possédons un système unique de distribution. La <strong>vente directe </strong> nous permet donc d'avoir des coûts réduits, sans distributeur, et donc de vous offrir des <strong>prix optimaux</strong>. <br> <br> Ainsi, nos produits sont certifiés par la <strong> CE</strong>, avec un emballage minimal, réutilisable et entièrement recyclable. En effet, les pots sont en verre de <strong>première qualité</strong>, la charnière en métal est composé d'un alliage d'inox permettant une <strong>durabilité</strong> à toute épreuve, et d'un caouchouc en matière <strong>naturelle</strong> 100% biodégradable. <br> <br> <span class="overthetop">Et bien sûr, tout ceci ne sert uniquement qu'à vous offrir la meilleure qualité d'air, car vous méritez de vivre un voyage, le temps d'un pshit !!!</span> </p> </span>
    </header>


    
    <h2 id="hpromo"> Promotions</h2>
    <p id="hpromop">Voici nos promotions du moment ! venez en profiter, pour un temps limité seulement !</p>

    <div id="Promotions">
    <div class="row">
     
    <?php foreach($products as $promos): ?>


            
      <div class="col-md-6 col-lg-4">
        <div class="thumbnail">

          <img class="img-fluid" src="<?=$promos ->UrlImage?>">
          <div class="promo"><?= $promos ->promo ?> %</div>
          <div class="caption">

            <h4><?= $promos ->Titre ?></h4>
            <p class="desc"><?= $promos ->Odeur ?></p>
            <p class="price"><span class="ancienprix"><?= $promos ->Prix ?> €</span> <br> <span class="nouveauprix"><?php $round = number_format($promos-> totalpromo, 2, ',', ' '); echo $round ?>€</span></p>
            <div class="flexbtn">
              <a href="#" class="btn btn-order" id="panier"  role="button"><i class="fas fa-shopping-cart"></i> Ajouter au panier</a>
              <a href="Produits.php" class="btn btn-order" id="produi" role="button"><i class="fas fa-plus"></i> d'infos</a>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach ?>


    </div>
  </div>


<br><br>
    <h2 id="hpromo"> Nouveautés</h2>   <!-- SELECT * FROM `produits` ORDER BY ID DESC LIMIT 3  -->
    <p id="hpromop">Voici nos nouveaux produits ! Cueillis directement par nos meilleurs chasseurs d'air, et récemment arrivés dans nos stocks !</p>

    <div id="Nouveautes">
    <div class="row">


  <?php foreach($nouveaux as $nouveau): ?>

      <div class="col-md-6 col-lg-4">
        <div class="thumbnail">

          <img class="img-fluid" src="<?= $nouveau ->UrlImage ?>" alt>
          <div class="promo">NEW</div>
          <div class="caption">

            <h4><?= $nouveau ->Titre ?></h4>
            <p class="desc"><?= $nouveau ->Odeur ?></p>
            <p class="price"><span class="nouveauprix"><?= $nouveau ->Prix ?> €</span></p>
            <div class="flexbtn">
              <a href="#" class="btn btn-order" id="panier"  role="button"><i class="fas fa-shopping-cart"></i> Ajouter au panier</a>
              <a href="Produits.php" class="btn btn-order" id="produi" role="button"><i class="fas fa-plus"></i> d'infos</a>
            </div>
          </div>


        </div>
      </div>

      <?php endforeach ?>


    </div>

  </div>

  </div> <!--CONTAINER-->

  

  


</main>

  <footer>
  <?php include("Php/footer.php"); ?>
  </footer>


  <script src="./Ressources/js/home.js"></script>
</body>


</html>