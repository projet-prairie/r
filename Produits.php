<!DOCTYPE html>
<html lang="fr-fr">
<head>


  <title>Produits - R</title>


</head>


<body>


<div class="Depart">
  <main>
    <?php include("Php/navbar.php"); ?>
   <h1 data-aos="fade-right">Nos produits</h1>
</div>

    <div class="container">
      <div class="row" data-aos="fade-up">

      <?php
try
{
       // On se connecte à MySQL et à la bonne bdd
       $bdd = new PDO('mysql:host=localhost;dbname=R', 'root', 'root');
}
catch(Exception $e)
{
       // En cas d'erreur, on affiche un message et on arrête tout
       die('Erreur : '.$e->getMessage());
}


// Si tout va bien, on peut continuer


// On récupère tout le contenu de la table produits
$reponse = $bdd->query('SELECT * FROM produits');


// On affiche chaque entrée une à une
while ($donnees = $reponse->fetch())
{
?>

        <div class="col-lg-3 col-md-5 col-sm-8 col-8 card" id="produits"> 
          <div class="card border-info mb-3"  id="cadre">
            <a data-fancybox href="<?php echo $donnees['UrlImage']; ?>" data-caption="<?php echo $donnees['Alt']; ?>">
            <img class="d-block w-100 round" id="imageProduit" data-caption="my Caption or whatever you want" src="<?php echo $donnees['UrlImage']; ?>" alt="<?php echo $donnees['Alt']; ?>">
            </a>  
              <div class="card-body">
                <div class="accordion accordion-flush" id="accordionFlushExample">
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-heading<?php echo $donnees['NumeroEnAnglais']; ?>">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?php echo $donnees['NumeroEnAnglais']; ?>" aria-expanded="false" aria-controls="flush-collapse<?php echo $donnees['NumeroEnAnglais']; ?>">
                        <?php echo $donnees['Titre']; ?>
                      </button>
                    </h2>
                    <div id="flush-collapse<?php echo $donnees['NumeroEnAnglais']; ?>" class="accordion-collapse collapse" aria-labelledby="flush-heading<?php echo $donnees['NumeroEnAnglais']; ?>" data-bs-parent="#accordionFlushExample">
                      <div class="accordion-body" style="align-items: center; justify-content: center;">
                        <div class="table-responsive">
                          <table class="table table-bordered" id="tableauProduit">
                            <thead>
                              <tr>
                                <th scope="col" id="col1" style="text-align: center; vertical-align: middle;"><i class="fas fa-music"></i> <U>Melodie</U>  <i class="fas fa-music"></i><br/><?php echo $donnees['Bruit']; ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="col" id="col2" style="text-align: center; vertical-align: middle;"><i class="fas fa-wind"></i> <U>Senteur</U> <i class="fas fa-wind"></i><br/><?php echo $donnees['Odeur']; ?></th>
                              </tr>
                              <tr>
                                <th scope="col" id="col3" style="text-align: center; vertical-align: middle;"><i class="fas fa-heartbeat"></i> <U>Emotion</U> <i class="fas fa-heartbeat"></i><br/><?php echo $donnees['Sentiment']; ?></th>
                              </tr>
                              <tr>
                                <th scope="col" id="col4" style="text-align: center; vertical-align: middle;"><!--<i class="fas fa-euro-sign"></i>--> <U>Prix</U> <!--<i class="fas fa-euro-sign"></i>--><br/><?php echo $donnees['Prix']; ?> €</th>
                              </tr>
                            </tbody>
                          </table>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
          </div>
        </div>

<?php
}
$reponse->closeCursor(); // Termine le traitement de la requête


?>
      
      </div>
    </div>

  </main>



  <footer>
    <?php include("Php/footer.php"); ?>
  </footer>
</body>


 


                                <!--JAVASCRIPT-->
<script src="Ressources/js/produits.js"></script>


</html>